var button;
var hr;
var a;
var layout = true;
var css1 = "style.css"
var css2 = "style2.css"
window.addEventListener("DOMContentLoaded", loaded);

function loaded(){
  button = document.getElementsByTagName("button")[0];
  button.addEventListener("click", buttonClicked);
  hr = document.getElementsByTagName("hr");
  a = document.getElementsByTagName("a")[0];
}

function buttonClicked(){
  if(layout){
    document.getElementsByTagName("link")[0].href = css2;
    for(i = 0; i < hr.length; i++){
      hr[i].style.visibility = "hidden";
    }
    a.style.visibility = "hidden";

    layout = false;
  }else{
    document.getElementsByTagName("link")[0].href = css1;
    for(i = 0; i < hr.length; i++){
      hr[i].style.visibility = "visible";
    }
    a.style.visibility = "visible";

    layout = true;
  }
}
